javascript:
  if (typeof _a === "undefined"){
  _a = document.body.appendChild(document.createElement('div'));
  Object.entries({
    padding:"10px",
    backgroundColor:"#ffffff",
    border:"1px solid #AAAAAA",
    "border-radius": "4px",
    position:"absolute",
    zIndex:20,
    top:"50px",
    width:"220px",
    height:"inherit"
  }).forEach(obj => {_a.style[obj[0]] = obj[1]});
  _a.innerText = 'Video Speed Modifier';
  _a.id = 'viv';
  _a.appendChild(document.createElement('input'))
  Object.entries({
    type: 'range',
    min: 25,
    max: 200,
    step: 25,
    value: 100
  }).forEach(obj => {_a.children[0][obj[0]] = obj[1]})
  _a.children[0].style.outline = null;
  _a.children[0].onchange = (evt) => {
    document.getElementsByTagName('video')[0].playbackRate = evt.explicitOriginalTarget.value/100
  }
  _a.appendChild(document.createElement('button'))
  _a.children[1].innerText = 'close'
  _a.children[1].onclick = () => {_a.remove()}
  Object.entries({
    float: 'right', 
    backgroundColor: '#ffffff', 
    border: '2px solid #ff4136', 
    "border-radius": 
    '5px', color: '#ff4136'
  }).forEach(obj => {_a.children[1].style[obj[0]] = obj[1]})
  _a.onmousedown = (evnt) => {
    if (event.target.id === 'viv') {
    evnt.preventDefault();
    dx = evnt.clientX;
    dy = evnt.clientY;
    document.addEventListener('mousemove',  _l = (evt) => {
      evnt.target.style.left = evnt.target.offsetLeft + (evt.clientX - dx)+'px';
      evnt.target.style.top = evnt.target.offsetTop + (evt.clientY - dy)+'px';
      dx = evt.clientX; dy = evt.clientY;
    })};
  };
  _a.onmouseup = (evnt) => {
    if (event.target.id === 'viv'){
      document.removeEventListener('mousemove', _l)
    }
  };
}else{
  document.body.appendChild(_a)
}
void 0;